import React from 'react';
import Button from './components/Button/Button';
import Stack from '@mui/material/Stack';
import { Typography } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { startTimer, clearTimers } from './Redux/slices/timerSlice';

const formatTime = (timestamp) => {
  const date = new Date(timestamp);
  return date.toTimeString().slice(0, 8); 
};

const App = () => {
  const dispatch = useDispatch();
  const timers = useSelector((state) => state.timer.timers);
  console.log(timers)

  const handleButtonClick = (seconds) => {
    dispatch(startTimer(seconds));
  };

  const handleClearClick = () => {
    dispatch(clearTimers());
  };

  return (
    <>
      <Stack sx={{ margin: '20px' }} direction="row" justifyContent="center" spacing={7}>
        <Button content='1 sec' seconds={1} clickFunction={() => handleButtonClick(1)} />
        <Button content='2 sec' seconds={2} clickFunction={() => handleButtonClick(2)} />
        <Button content='3 sec' seconds={3} clickFunction={() => handleButtonClick(3)} />
        <Button content='CLEAR' clickFunction={handleClearClick} />
      </Stack>

      <Stack sx={{ margin: '20px', border: '1px solid black', borderRadius: '20px', padding: '10px', height: '500px', overflow: 'hidden' }} spacing={1}>
        {timers.map(({ seconds, startTime}, index) => (
          <Typography key={index} variant="h6">{`Button #${seconds}: ${formatTime(startTime)} - ${formatTime(startTime + seconds * 1000)} (${seconds} sec)`}</Typography>
        ))}
      </Stack>
    </>
  );
};

export default App;

