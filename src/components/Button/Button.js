import React, { memo } from 'react';
import { Button } from '@mui/material';

import PropTypes from 'prop-types';

const CustomButton = ({ content, clickFunction }) => {
  return (
    <Button sx={{ width: '300px' }} onClick={clickFunction} variant="contained">
      {content}
    </Button>
  );
};

CustomButton.propTypes = {
  content: PropTypes.string,
  clickFunction: PropTypes.func,
};

CustomButton.defaultProps = {
  content: '',
  clickFunction: () => {},
};

export default memo(CustomButton);