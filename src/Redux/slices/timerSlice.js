import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const startTimer = createAsyncThunk(
  'timer/startTimer', 
  async (seconds, { dispatch }) => {
  const startTime = Date.now();
  dispatch(timerSlice.actions.startTimer({ seconds, startTime }));
});

const initialState = {
  timers: [],
};

const timerSlice = createSlice({
  name: 'timer',
  initialState,
  reducers: {
    startTimer: (state, action) => {
      const { seconds, startTime } = action.payload;
      console.log(action.payload)
      state.timers.push({ seconds, startTime });
    },
    clearTimers: (state) => {
      state.timers = [];
    },
  },
});

export const { removeTimer, clearTimers } = timerSlice.actions;
export default timerSlice.reducer;


// import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// export const startTimer = createAsyncThunk(
//   'timer/startTimer', 
//   async (seconds, { dispatch }) => {
//     const startTime = Date.now();

//     dispatch(timerSlice.actions.changeTimer(0));
//     dispatch(timerSlice.actions.startTimer({ seconds, startTime }));

//   }
// );

// const initialState = {
//   timers: [],
//   time: 0,
// };

// const timerSlice = createSlice({
//   name: 'timer',
//   initialState,
//   reducers: {
//     startTimer: (state, action) => {
//       const { seconds, startTime } = action.payload;
//       const value = state.time
//       state.timers.push({ seconds, startTime, value });
//     },
//     changeTimer: (state, action) => {
//       state.time += action.payload;
//       console.log(state.timers)
//     },
//     clearTimers: (state) => {
//       state.timers = [];
//       state.time = 0;
//     },
//   },
// });

// export const { removeTimer, clearTimers } = timerSlice.actions;
// export default timerSlice.reducer;
